/*
** EPITECH PROJECT, 2020
** sprite_menu
** File description:
** create_sprite_menu
*/

#include "../../my_rpg.h"

void create_sprite_menu(game_t *game)
{
    game->menu->sprite_fond = sfSprite_create();
    game->menu->start_sprite = sfSprite_create();
}

void create_texture_menu(game_t *game)
{
    game->menu->texture_fond = sfTexture_createFromFile("./game/menu/png/menu_fond.png", NULL);
    game->menu->start_texture = sfTexture_createFromFile("./game/menu/png/start.png", NULL);
}

void set_texture_menu(game_t *game)
{
    sfSprite_setTexture(game->menu->sprite_fond, game->menu->texture_fond, sfTrue);
    sfSprite_setTexture(game->menu->start_sprite, game->menu->start_texture, sfTrue);
    
}

void draw_texture_menu(game_t *game)
{
    sfRenderWindow_drawSprite(game->window->window, game->menu->sprite_fond, NULL);
    sfRenderWindow_drawSprite(game->window->window, game->menu->start_sprite, NULL);
}

void init_sprite_menu(game_t *game)
{
    create_sprite_menu(game);
    create_texture_menu(game);
    set_texture_menu(game);
}