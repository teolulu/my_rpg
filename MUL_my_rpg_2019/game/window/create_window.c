/*
** EPITECH PROJECT, 2020
** create 
** File description:
** create_window
*/

#include "../../my_rpg.h"

void analyse_events(game_t *game)
{
    if (game->window->event.type == sfEvtClosed)
        sfRenderWindow_close(game->window->window);
}

void init_window(game_t *game)
{
    init_sprite_menu(game);
    game->window->video.width = 1920;
    game->window->video.height = 1080;
    game->window->window = sfRenderWindow_create(game->window->video,
        "MY_RPG", sfResize | sfClose, NULL);
}

void create_window(game_t *game)
{
    init_window(game);
    while (sfRenderWindow_isOpen(game->window->window)) {
        while (sfRenderWindow_pollEvent(game->window->window, &game->window->event)) {
            analyse_events(game);
        }
        sfRenderWindow_clear(game->window->window, sfBlack);
        open_scene_game(game);
        //draw_texture_menu(game);
        sfRenderWindow_display(game->window->window);
    }
}