/*
** EPITECH PROJECT, 2020
** my_rpg.h
** File description:
** my_rpg
*/

#ifndef MY_RPG_H_
#define MY_RPG_H_

#include <SFML/Graphics.h>
#include <stdlib.h>
#include <time.h>
#include <SFML/Audio.h>
#include <stdio.h>
#include <SFML/Window.h>
#include <SFML/System.h>
#include <SFML/Audio.h>
#include <string.h>

typedef struct window_s
{
    int scene;
    sfRenderWindow *window;
    sfVideoMode video;
    sfEvent event;
} window_t;

typedef struct menu_s
{
    sfSprite *sprite_fond;
    sfTexture *texture_fond;

    sfSprite *start_sprite;
    sfTexture *start_texture;
} menu_t;

typedef struct game_s
{
    window_t *window;
    menu_t *menu;
} game_t;

void create_sprite_menu(game_t *game);
void create_texture_menu(game_t *game);
void set_texture_menu(game_t *game);
void draw_texture_menu(game_t *game);
void init_sprite_menu(game_t *game);
void analyse_events(game_t *game);
void init_window(game_t *game);
void create_window(game_t *game);
void open_scene_game(game_t *game);

#endif /* !MY_RPG_H_ */
